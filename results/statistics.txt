Unique words = words left after stop word removal and removing punctuation. Words are not case sensitive.


------------------------------
Data structures and algorithms

1. Difficult topics
Answers: 25
Unique words: 120
Unique stemmed words: 105
Words in average answer: 6.28

2. Best features
Answers: 24
Unique words: 95
Unique stemmed words: 83
Words in average answer: 5.75

3&4. Improvements and comments
Answers: 34
Unique words: 210
Unique stemmed words: 179
Words in average answer: 9.94

-----------------------------------------------
Communication skills, IT skills, law and ethics

1. Difficult topics
Answers: 46
Unique words: 175
Unique stemmed words: 159
Words in average answer: 6.67

2. Best features
Answers: 40
Unique words: 137
Unique stemmed words: 112
Words in average answer: 5.75

3&4. Improvements and comments
Answers: 43
Unique words: 236
Unique stemmed words: 202
Words in average answer: 9.37

---------------------
Introductory security

1. Difficult topics
Answers: 36
Unique words: 136
Unique stemmed words: 119
Words in average answer: 7.19

2. Best features
Answers: 32
Unique words: 142
Unique stemmed words: 125
Words in average answer: 7.84

3&4. Improvements and comments
Answers: 30
Unique words: 201
Unique stemmed words: 179
Words in average answer: 10.53

-------------------------
Tepe / välipalautekysely

1. Miksi kävit/et käynyt luennoilla?
Answers: 42
Unique words: 349
Unique stemmed words: 228
Words in average answer: 11.76

4. Miksi kävit/et käynyt harjoituksissa?
Answers: 42
Unique words: 282
Unique stemmed words: 178
Words in average answer: 8.69

7&8. Kiitokset & Parannusehdotuksia
Answers: 42
Unique words: 1116
Unique stemmed words: 658
Words in average answer: 40.38

------
Viscos

55. Mitä ohjelmointikieliä osaat
Answers: 92
Unique words: 544
Unique stemmed words: 297
Words in average answer: 12.14

57. Ongelmia kurssilla
Answers: 96
Unique words: 644
Unique stemmed words: 388
Words in average answer: 10.17

26&28. Vanhemmat
Answers: 95
Unique words: 749
Unique stemmed words: 468
Words in average answer: 14.25

54&55. Ohjelmointi
Answers: 95
Unique words: 1149
Unique stemmed words: 613
Words in average answer: 23.93

